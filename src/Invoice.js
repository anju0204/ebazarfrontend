import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Invoice extends React.Component{
  render(){
    return(
      <div>
      <div className="wrapper">
        <section className="invoice">
      <div className="row">
      <div className="col-12">
        <h2 className="page-header">
         
          <small className="float-right">Date: 2/10/2014</small>
        </h2>
      </div>
   </div>
   <div className="row invoice-info">
     <div className="col-sm-4 invoice-col">
       From
     <address>
          <strong>Admin, Inc.</strong><br/>

          San Francisco, CA 94107<br/>
          Phone: (804) 123-5432<br/>
          Email: info@almasaeedstudio.com
        </address>
     </div>
     <div className="col-sm-4 invoice-col">
        To
        <address>
          <strong>Rashmi Gairhe</strong><br/>
          
          Tanahun Vimad<br/>
          Phone: 9867736478<br/>
          Email: rashmigairhe7@gmail.com
        </address>
      </div>
      <div className="col-sm-4 invoice-col">
        <b>Invoice #007612</b><br/>
        <br/>
        <b>Order ID:</b> 4F3S8J<br/>
        <b>Payment Due:</b> 2/22/2014<br/>
        <b>Account:</b> 968-34567<br/>
      </div>
      
    <div className="row">
      <div className="col-12 table-responsive">
        <table className="table table-striped">
          <thead>
          <tr>
            <th>Qty</th>
            <th>Product</th>
            <th>Serial #</th>
            <th>Description</th>
            <th>Subtotal</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>1</td>
            <td>Jacket</td>
            <td>455-981-221</td>
            <td>Very comfortable</td>
            <td>$64.50</td>
          </tr>
          
          <tr>
            <td>1</td>
            <td>Pant</td>
            <td>735-845-642</td>
            <td>Levis pant imported from USA</td>
            <td>$10.70</td>
          </tr>
         
          </tbody>
        </table>
      </div>
    
    </div>
    <div className="row">

     
     <div className="col-6">
       <p className="lead">Amount Due 2/22/2014</p>

       <div className="table-responsive">
         <table className="table">
           <tr>
             <th style={{width:"50%"}}>Subtotal:</th>
             <td>$250.30</td>
           </tr>
           <tr>
             <th>Tax (9.3%)</th>
             <td>$10.34</td>
           </tr>
           <tr>
             <th>Shipping:</th>
             <td>$5.80</td>
           </tr>
           <tr>
             <th>Total:</th>
             <td>$265.24</td>
           </tr>
         </table>
       </div>
     </div>
    
   </div>

   </div>
    
    </section>
      </div>
      </div>
    );
  }
}
export default Invoice;