import React, { Component, Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
// const Homescreen= React.lazy(()=> import('./Local/Components/Homescreen')); 
// // const Topbar= React.lazy(()=> import('./Local/Topbar'));
// const Sidebar= React.lazy(()=> import('./Local/Sidebar')); 
// const SignIn= React.lazy(()=> import('./Local/SignIn'));  
// // const SignIn= React.lazy(()=> import('./Local/SignIn')); 

import SignIn from './Local/SignIn';
import Sidebar from './Local/Sidebar';
import Topbar from './Local/Topbar';
import Homescreen from './Local/Components/Homescreen';
// import Electronics from './Local/Components/Brands/Electronics';
// import Invoice from '../src/Invoice';
// import Ecommernce from './Local/Components/Brands/Ecommernce';
// import Login from './Login';
// import Register from './Register';
// class App extends Component {
//   render() {
//     return (
//       <Suspense fallback={<div className="centered"></div>}>
//         <Router>
//           <Homescreen/>
//         </Router>
//       </Suspense>
//     );
//   }
// }
// class App extends React.Component {
// 	render() {
// 	  return (
// 		<div>
// 	   <Sidebar />
// 	  <Topbar />
//    <Homescreen/>
// 		</div>
// 	  );
// 	}
//   }

const DefaultContainer = () => (
    <div>
  
             <Router>
				 <Topbar />
	<Sidebar />
              <Switch>
				 
			  <Route path="/homescreen" exact render={() => <Homescreen />} />
			  
					
				{/* <Route path="/electronics" exact render={() => <Electronics />} />
				<Route path="/invoice" exact render={() => <Invoice />} />
				<Route path="/ecommernce" exact render={() => <Ecommernce />} /> */}
             </Switch>
            </Router>
   
    </div>
 )


class App extends React.Component {
  render() {
    return (
      <div>
		  <Suspense fallback={<div><p></p></div>}></Suspense>
 <Router>
	 <Switch>
				 <Route path="/" exact render={() => <SignIn />} />
				
				 <Route component={DefaultContainer}/>
				
			</Switch>
		 {/* <Footer />  */}
		</Router>
      </div>
    );
  }
}

export default App;

