import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Electronis extends  React.Component {

  render() {
    return (
      <div>
<div class="wrapper">


  <div class="preloader flex-column justify-content-center align-items-center">
  
  </div>
  </div>
    <div class="content-wrapper">
     
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Grab The Offer</h3>
              </div>
           
              <div class="card-body">
              
                <div class="row mt-4">
                  <div class="col-sm-4">
                    <div class="position-relative">
                      <img src="./assets/img/photo1.png" alt="Photo 1" class="img-fluid"/>
                      <div class="ribbon-wrapper ribbon-lg">
                        <div class="ribbon bg-success text-lg">
                          30% off
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="position-relative">
                      <img src="./assets/img/photo2.png" alt="Photo 2" class="img-fluid"/>
                      <div class="ribbon-wrapper ribbon-xl">
                        <div class="ribbon bg-warning text-lg">
                          50% off
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="position-relative" >
                      <img src="./assets/img/photo3.jpg" alt="Photo 3" class="img-fluid"/>
                      <div class="ribbon-wrapper ribbon-xl">
                        <div class="ribbon bg-danger text-lg">
                          70% off
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            
            </div>
         
          </div>
         
        </div>
     
      </div>
    
    </section>
      </div>
        </div>
    );
  }
}

export default Electronis;